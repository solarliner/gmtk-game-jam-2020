# Attribution

Within the project, the following assets were used:

## Music

- I Think I Can Help You - The Six Realms
- pATCHES - Hard To Let Go Of Grammar
- pATCHES - Because For Everything There Is Someone
- The Whole Other - Beyond The Lows

## SFX

- "Rain on Windows, Interior, B.wav" by InspectorJ (www.jshaw.co.uk) of Freesound.org
- Soundkampf, "Nice city atmosphere"
- Risto Alcinov, "Ventilation, Walla, Elevator, Residential Hallway"
- Excerpt of GMTK's video, "Are Western and Japanese RPGs so Different? | Design Icons"

## Graphics

- Original artwork by dqaxd

## Fonts

- Cyreal, "Lora"