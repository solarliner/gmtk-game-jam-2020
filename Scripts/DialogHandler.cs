using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Godot;
using JsonLite;
using JsonLite.Ast;

namespace GMTKGameJam2020.Scripts
{
    public class DialogHandler: Node2D
    {
        [Export] public string basePath;
        [Export] public string levelPath;
        public static DialogHandler Instance { get; private set; }
        public Dictionary<string, string[]> Dialogs { get; private set; }
        private Random _random;
        public override void _Ready()
        {
            Instance = this;
            _random = new Random();
            Dialogs = new Dictionary<string, string[]>();
            LoadDialogs(basePath);
            LoadDialogs(levelPath);
            /*GD.Print("Dialogs:");
            foreach (var kvp in Dialogs)
            {
                GD.Print($"\tDialogs for {kvp.Key}:");
                foreach (var line in kvp.Value)
                {
                    GD.Print($"\t\t{line}");
                }
            }*/
        }

        private void LoadDialogs(string path)
        {
            var file = new Godot.File();
            file.Open(path, Godot.File.ModeFlags.Read);
            MergeDialogs(file.GetAsText());
        }

        public void DoStartDialog()
        {
            Say("_start");
        }

        public void DoWinDialog()
        {
            Say("_win");
        }

        public void DoLoseDialog()
        {
            Say("_timeout");
        }

        public void Say(string key)
        {
            GD.Print($"Say({key})");
            if (!Dialogs.ContainsKey(key)) return;
            GD.Print("\tDialogs has key");
            var dialogs = Dialogs[key];
            int idx = _random.Next(dialogs.Length);
            DialogueBox.Instance.ShowDialog(dialogs[idx]);
        }

        private void MergeDialogs(string dialogsJson)
        {
            var parser = new JsonAstParser(new JsonTextReader(new StringReader(dialogsJson)));
            var root = parser.CreateAst();
            if (!(root is JsonObject rootObject)) return;
            foreach(var member in rootObject.Members)
            {
                switch (member.Value)
                {
                    case JsonString value:
                        Dialogs.Add(member.Name, new []{value.Value});
                        break;
                    case JsonArray array:
                        Dialogs.Add(member.Name, array.OfType<JsonString>().Select(js => js.Value).ToArray());
                        break;
                }
            }
        }
    }
}