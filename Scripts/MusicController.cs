using Godot;
using System;

public class MusicController : Node2D
{
    [Export] public AudioStream[] streams;
    private AudioStreamPlayer _player;
    public AudioStream CurrentStream => streams[_curIndex];
    private Random _random;
    private uint _curIndex = uint.MaxValue;
    private bool[] _played;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _player = GetChild<AudioStreamPlayer>(0);
        _played = new bool[streams.Length];
        _random = new Random();
        Next();
        _player.Connect("finished", this, nameof(OnPlayerFinished));
        Play(CurrentStream);
    }

    public void Next()
    {
        uint nextIndex = _curIndex;
        while (nextIndex == _curIndex) nextIndex = (uint) _random.Next(streams.Length);
        _curIndex = nextIndex;
        Play(CurrentStream);
        GD.Print($"Playing stream index {_curIndex} ({CurrentStream.ResourceName} @ {CurrentStream.ResourcePath})");
    }

    public void Pause()
    {
        _player.Playing = false;
    }

    public void Resume()
    {
        _player.Playing = true;
    }

    private void OnPlayerFinished()
    {
        Next();
    }

    private void Play(AudioStream stream)
    {
        if (_player.Playing)
            _player.Stop();
        _player.Stream = stream;
        _player.Play();
    }
}