using Godot;
using System;
using System.Linq;
using GMTKGameJam2020.Scripts;
using Godot.Collections;
using Array = Godot.Collections.Array;

public class Furniture : Sprite
{
    [Signal]
    public delegate void ItemClicked(Furniture thisNode);
    private Area2D _area;
    private Label _label;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _area = GetChild<Area2D>(0);
        _area.Connect("mouse_entered", this, nameof(OnMouseEntered));
        _area.Connect("mouse_exited", this, nameof(OnMouseExited));
        _area.Connect("input_event", this, nameof(OnInputEvent));

        _label = GetNode<Label>("CanvasLayer/Label");
        _label.Text = Name;
        _label.Visible = false;
    }

    public override void _Process(float delta)
    {
        if (!_label.Visible) return;
        _label.RectPosition = GetGlobalTransformWithCanvas().origin;
    }

    private void OnInputEvent(Viewport _viewport, InputEvent @event, object _showIdx)
    {
        if (@event is InputEventMouseButton buttonEvent && buttonEvent.Pressed &&
            buttonEvent.ButtonIndex == (int) ButtonList.Left)
        {
            EmitSignal(nameof(ItemClicked), this);
        }
    }

    private void OnMouseEntered()
    {
        _label.Visible = true;
    }

    private void OnMouseExited()
    {
        _label.Visible = false;
    }
}
