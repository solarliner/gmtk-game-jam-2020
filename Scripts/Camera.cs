using Godot;
using System;

public class Camera : Camera2D
{
    [Export] public float effect = 2f;

    [Export(PropertyHint.Range, "0, 1, 0.1")]
    public float zoomFactor = 0.1f;

    [Export] public float minZoom = 0.1f;

    [Export] public float maxZoom = 3f;

    private Vector2 _basePos;
    private Vector2? _mousePos = null;
    private Vector2 _mouseVel;
    private Vector2 _targetZoom;
    private bool _dragging;

    public override void _UnhandledInput(InputEvent @event)
    {
        switch (@event)
        {
            case InputEventMouseMotion mouseMotion:
            {
                if (_mousePos.HasValue)
                    _mouseVel = mouseMotion.Position - _mousePos.Value;
                _mousePos = mouseMotion.Position;
                break;
            }
            case InputEventMouseButton eventMouse:
            {
                _dragging = (eventMouse.ButtonIndex & (int) ButtonList.Left) != 0 && eventMouse.Pressed;
                _targetZoom += zoomFactor * eventMouse.ButtonIndex switch
                {
                    (int) ButtonList.WheelDown => -Vector2.One,
                    (int) ButtonList.WheelUp => Vector2.One,
                    _ => Vector2.Zero
                };
                if (_targetZoom.x < minZoom) _targetZoom = Vector2.One * minZoom;
                else if (_targetZoom.x > maxZoom) _targetZoom = Vector2.One * maxZoom;
                break;
            }
        }
    }

    public override void _Ready()
    {
        _basePos = GlobalPosition;
        _mousePos = Vector2.Zero;
        _targetZoom = Zoom;
    }

    public override void _Process(float delta)
    {
        Zoom = Zoom.LinearInterpolate(_targetZoom, 0.2f);

        if (_dragging) _basePos -= _mouseVel * Zoom;
        _mouseVel = Vector2.Zero;

        Rect2 view = GetViewportRect();
        if (!_mousePos.HasValue || _dragging) GlobalPosition = _basePos;
        else
        {
            Vector2 displacement = Normalize(view, _mousePos.Value);
            GlobalPosition = _basePos + effect * displacement;
        }
    }

    private static Vector2 Normalize(Rect2 rect, Vector2 pt)
    {
        Vector2 local = pt - rect.Position;
        float relX = 2f * (local.x - rect.Size.x / 2f) / rect.Size.x;
        float relY = 2f * (local.y - rect.Size.y / 2f) / rect.Size.y;
        return new Vector2(relX, relY);
    }
}