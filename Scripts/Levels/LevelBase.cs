using System.Linq;
using Godot;

public abstract class LevelBase : Node2D
{
    public static LevelBase GetLevel(Node node) => node.GetViewport().GetChildren().OfType<LevelBase>().First();
    public abstract void StartLevel();
    public abstract void LevelTimeout();
}