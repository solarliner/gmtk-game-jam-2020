using System.Collections.Generic;
using Godot;
using TransitionType = Godot.Tween.TransitionType;
using System.Linq;
using GMTKGameJam2020.Scripts;
using Godot.Collections;

public class Level1 : LevelBase
{
    [Signal]
    public delegate void LevelComplete();

    [Export] public PackedScene levelCompleteScene;
    private Tween _levelTween;
    private Timer _levelTimer;

    private bool _trashTaken = false;
    private bool _keyTaken = false;
    private bool _socksOn = false;
    private bool _shoesOn = false;

    public override void _Ready()
    {
        Array children = GetChildren();
        _levelTween = children.OfType<Tween>().First();
        _levelTimer = children.OfType<Timer>().First();
        AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("SFX"), float.NegativeInfinity);
        foreach (var node in GetAllChildren(this).OfType<Furniture>())
        {
            //GD.Print($"Node name: {node.Name}");
            node.Connect(nameof(Furniture.ItemClicked), this, nameof(ItemClicked));
        }
    }

    public override void StartLevel()
    {
        _levelTween.InterpolateMethod(this, nameof(ChangeSFXVolume), 0f, 1f, 3f, TransitionType.Expo);
        _levelTween.Connect("tween_all_completed", this, nameof(TweenComplete));
        _levelTween.Start();
    }

    private void ChangeSFXVolume(float vol)
    {
        int busIdx = AudioServer.GetBusIndex("SFX");
        float dB = 20f * Mathf.Log(vol) / Mathf.Log(10f);
        AudioServer.SetBusVolumeDb(busIdx, dB);
    }

    public override void LevelTimeout()
    {
        GD.Print("Level timeout");
    }

    private void TweenComplete()
    {
        DialogHandler.Instance.DoStartDialog();
    }

    private void ItemClicked(Furniture node)
    {
        switch (node.Name)
        {
            case "Trash":
                _trashTaken = true;
                DialogHandler.Instance.Say("Trash");
                node.QueueFree();
                break;
            case "Key":
                _keyTaken = true;
                DialogHandler.Instance.Say("Key");
                node.QueueFree();
                break;
            case "Socks":
                _socksOn = true;
                DialogHandler.Instance.Say("Socks");
                node.QueueFree();
                break;
            case "Shoes":
                if (_socksOn)
                {
                    _shoesOn = true;
                    DialogHandler.Instance.Say("Shoes");
                    node.QueueFree();
                }
                else
                {
                    DialogHandler.Instance.Say("Shoes:unable");
                }

                break;
            case "Door":
                if (_shoesOn && _trashTaken && _keyTaken)
                {
                    DialogHandler.Instance.DoWinDialog();
                    DoWin();
                }
                else
                {
                    DialogHandler.Instance.Say("Door:unable");
                }
                break;
            default:
                DialogHandler.Instance.Say(node.Name);
                break;
        }
    }

    private void DoWin()
    {
        EmitSignal(nameof(LevelComplete));
        FindNode("CanvasLayer").AddChild(levelCompleteScene.Instance());
    }

    private static IEnumerable<Node> GetAllChildren(Node node, bool root = true)
    {
        if (!root) yield return node;
        foreach (var child in node.GetChildren().OfType<Node>().SelectMany(n => GetAllChildren(n, false)))
        {
            yield return child;
        }
    }
}
