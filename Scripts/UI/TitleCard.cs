using Godot;
using System;
using System.Linq;

public class TitleCard : Panel
{
    [Signal] public delegate void Play();

    [Signal]
    public delegate void Complete();

    [Export] public NodePath tween;
    [Export] public PackedScene creditsScene;

    private Tween _fadeTween;
    private CanvasModulate _modulate;

    public override void _Ready()
    {
        _modulate = GetChildren().OfType<CanvasModulate>().First();
        _fadeTween = GetNode<Tween>(tween);
    }

    public void DoPlay()
    {
        _fadeTween.InterpolateMethod(this, nameof(UpdateOpacity), 1f, 0f, 1f);
        _fadeTween.Connect("tween_all_completed", this, nameof(DoComplete));
        _fadeTween.Start();
        EmitSignal(nameof(Play));
    }

    public void DoCredits()
    {
        // TODO: Credits UI load
        AddChild(creditsScene.Instance());
    }

    private void UpdateOpacity(float opacity)
    {
        GD.Print($"Update opacity: {opacity}");
        var newColor = new Color(_modulate.Color.ToRgba64()) {a = opacity};
        _modulate.Color = newColor;
    }

    private void DoComplete()
    {
        EmitSignal(nameof(Complete));
        QueueFree();
    }
}
