using Godot;
using System;

public class TransitionToMenu : Control
{
    [Export] private string nextScene;
    public override void _Ready()
    {
        GetTree().ChangeScene(nextScene);
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
