using Godot;
using System;

public class Credits : Panel
{
    public override void _GuiInput(InputEvent @event)
    {
        GD.Print($"Event: {@event}");
        if(@event is InputEventMouseButton mouseButton && mouseButton.Pressed && mouseButton.ButtonIndex == (int)ButtonList.Left)
            QueueFree();
    }
}
