using Godot;
using System;
using System.Linq;
using Array = Godot.Collections.Array;

public class Level1Complete : Panel
{
    [Export] public string mainMenuScenePath;
    private CanvasModulate _modulate;
    public override void _Ready()
    {
        Array children = GetChildren();
        var tween = children.OfType<Tween>().First();
        _modulate = children.OfType<CanvasModulate>().First();
        tween.InterpolateMethod(this, nameof(ChangeOpacity), 0f, 1f, 1f);
        tween.Start();
    }

    public void ReturnToMainMenu()
    {
        var err = GetTree().ChangeScene(mainMenuScenePath);
        GD.Print($"Error?: {err}");
    }

    private void ChangeOpacity(float value)
    {
        _modulate.Color = new Color(_modulate.Color.ToRgba64()) {a=value};
    }
}
