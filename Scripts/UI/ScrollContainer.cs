using Godot;
using System;

public class ScrollContainer : Godot.ScrollContainer
{
    [Export] public float scrollSpeed;
    private float _scrollAmt;
    public override void _Process(float delta)
    {
        _scrollAmt += delta * scrollSpeed;
        ScrollVertical = (int) _scrollAmt;
    }

    public override void _GuiInput(InputEvent @event)
    {
        if(@event is InputEventMouseButton mouseButton && mouseButton.Pressed && mouseButton.ButtonIndex == (int)ButtonList.Left)
            GetParent().QueueFree();
    }
}
