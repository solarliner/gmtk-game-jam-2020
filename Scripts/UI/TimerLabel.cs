using Godot;
using System;
using System.Linq;

public class TimerLabel : Label
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private Timer _levelTimer;

    public override void _Ready()
    {
        _levelTimer = LevelBase.GetLevel(this).GetChildren().OfType<Timer>().First();
    }

    public override void _Draw()
    {
        var span = new TimeSpan(TimeSpan.TicksPerMillisecond * (long) (_levelTimer.TimeLeft * 1000f));
        Text = span.ToString("c");
    }
}