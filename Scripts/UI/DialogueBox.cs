using Godot;
using System;

public class DialogueBox : Panel
{
    public static DialogueBox Instance { get; private set; }
    private Label _label;
    private Timer _timer;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _label = GetNode<Label>("MarginContainer/Label");
        Instance = this;
        _timer = GetNode<Timer>("Timer");
        _timer.Connect("timeout", this, nameof(OnTimerTimeout));

        // Set initial state
        OnTimerTimeout();
    }

    public void ShowDialog(string message)
    {
        _timer.Start();
        _label.Text = message;
        Visible = true;
        MouseFilter = MouseFilterEnum.Stop;
    }

    private void OnTimerTimeout()
    {
        Visible = false;
        MouseFilter = MouseFilterEnum.Ignore;
        _timer.Stop();
    }
}
