using Godot;
using System;

public class MenuButton : Button
{
    [Export] public PackedScene mainScene;
    public override void _Ready()
    {
        Connect("pressed", this, nameof(ButtonPressed));
    }

    private void ButtonPressed()
    {
        GetTree().ChangeSceneTo(mainScene);
    }
}
