using Godot;

public class TVLight : Light2D
{
    [Export] public Texture colors;
    private Image _colorsData;
    private int _tick;
    private int _imageSize;

    public override void _Ready()
    {
        _colorsData = colors.GetData();
        _imageSize = (int) colors.GetSize().x;
        _colorsData.Lock();
    }

    public override void _ExitTree()
    {
        _colorsData.Unlock();
    }

    public override void _Process(float delta)
    {
        Color = _colorsData.GetPixel(_tick++, 0);
        _tick %= _imageSize;
    }
}